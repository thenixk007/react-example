import React from 'react';
import ReactDOM from 'react-dom';
import { createStore } from 'redux';
import { Provider } from 'react-redux'
import configureStore from './store';
import usersReducer from './reducers/usersReducer';

import './index.css';
import App from './App';


const store = createStore(usersReducer);//สร้างคลังข้อมูล


ReactDOM.render(
    <Provider store={configureStore()}>
        <App />
    </Provider>, document.getElementById('root')
);





