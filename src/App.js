import React, { Component } from 'react';
import ShowUsers from './ShowUsers';
import { BrowserRouter, Route } from 'react-router-dom';
import './App.css'; 
import { from } from 'rxjs';
import Navbar from './components/Navbar';
import Footer from './components/Footer';
import Detail from './components/Detail';

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div className=''>
            <Navbar/>
            <Route exact path="/" component={ShowUsers}/>
            <Route path="/:login" component={Detail}/>
            {/* <ShowUsers/> */}
            <Footer/>
        </div>      
      </BrowserRouter>
    )
  }
}

export default App;
