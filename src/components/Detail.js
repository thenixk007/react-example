import React, { Component } from 'react'
import axios from 'axios'




const apiUrl = 'https://api.github.com/users/'

class Detail extends Component {
    state = {
        user: [],
        repos: [],
        // user: null,
        // repos: null,
    }
    componentDidMount() {
        let login = this.props.match.params.login
        axios.get(apiUrl + login)
        .then(res => {
            this.setState({
                user: res.data  
            })
            console.log(res)
        })

        let username = this.props.match.params.login
        axios.get(apiUrl + username + '/repos')
        .then(res => {
            this.setState({
                repos: res.data
            })
            console.log(res) 
        })
        // console.log(this.state.user) 
    }
    render() {           

        const user = this.state.user ? ( 
            <div className='user'>
                <img src={this.state.user.avatar_url} />
                <p> <strong> Name : </strong> {this.state.user.name}</p>                
            </div>
        ) : (
            <div className="wrapper-con-load">
                  <div className="wrapper-loading">
                    <div className="loader"></div>
                    <strong>Loading</strong>
                  </div>
              </div>
        )        

        const repos = this.state.repos ? ( 
            <div className='user'>
                <p><strong> Repository : </strong> {this.state.repos.length}</p>               
            </div>
        ) : (
            <div>
                
            </div>
        )    

        this.items = this.state.repos.map((item, key) =>
            <div className='repos'>   
                <div key={item.id}>
                    <strong> {key + 1} . Name :: </strong> {item.name}
                </div>
                <div key={item.id}>  
                    <strong> Description :: </strong> 
                    {item.description}                                  
                </div>                                            
            </div>
        );    
            
        return(
            <div>
                <div>
                    { user }                                                                                                                        
                    { repos }
                    <div>
                        { this.items }                                                                                                                                                                                                                                                                                             
                    </div>                                                                                                                                      
                </div>
            </div>
        )
    }
}

export default Detail;