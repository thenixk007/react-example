import React, { Component } from 'react';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import axios from 'axios';
import { Link } from 'react-router-dom'; 

import { connect } from 'react-redux';
import { simpleAction } from './actions/simpleAction';

const apiUrl = 'https://api.github.com/users';

const mapStateToProps = state => ({
  ...state
 })

 const mapDispatchToProps = dispatch => ({
  simpleAction: () => dispatch(simpleAction())
 })
 
 
class ShowUsers extends Component {

  state = {
    users: [],
  }

  componentDidMount() {
     axios.get(apiUrl)
      .then(res => {
        const users = res.data;
        this.setState({ users });
      })
  }
  render() {
    if(this.state.users.length === 0) {
      return  <div className="wrapper-con-load">
                  <div className="wrapper-loading">
                    <div className="loader"></div>
                    <strong>Loading</strong>
                  </div>
              </div>
    }
    return (
      <div className='App'>
          <input className='input-search' placeholder='Seach'/>
          <div className='wrapper'>
              {
                this.state.users.map((user) => (
                  <div className='wrapper-card'>
                      <Link to={'/' + user.login }>
                      <img src={user.avatar_url} />
                      </Link>
                      <p>NAME : {user.login}</p>
                      <p>URL : {user.url}</p>                    
                  </div>
                ))
              }
          </div>
      </div>
    ) 
  }
}

export default connect(mapStateToProps, mapDispatchToProps) (ShowUsers);
